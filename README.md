Program allowing creation, modification and removal of Symbolic Links.

Minimal console interface:

-c Command::CreateReparsePoint

-o Command::OverwriteReparsePoint

-s Command::SetNewTarget

-u Command::UpdateTarget

-g Command::GetCurrentTarget

-d Command::DeleteReparsePoint



-p "<Reparse Point Path>"

-t "<Reparse Point Target Path>"


GetCurrentTarget displays reparse point target path in the console (standard output).

Project created for VS2013 (tools v100). Should support Windows systems starting with Vista.