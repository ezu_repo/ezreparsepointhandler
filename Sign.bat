cd /d "%~dp0"
set _signKey=%~2
if [%_signKey%]==[] set _signKey=%VS_BUILD_COMMON_KEY_FILE%
if [%_signKey%]==[] set _signKey=\\192.168.189.8\Projects\_CommonSetup\_CodeSigningCerticiate\EZUniverseInc.2014.pfx

signtool.exe sign /f "%_signKey%" /p universe "%~1"
if %ERRORLEVEL% gtr 0 exit /b 1

REM signtool.exe timestamp /t "http://timestamp.verisign.com/scripts/timestamp.dll" "%~1"
for %%s in (http://timestamp.verisign.com/scripts/timstamp.dll http://timestamp.comodoca.com/authenticode http://timestamp.globalsign.com/scripts/timestamp.dll http://tsa.starfieldtech.com) do (
	signtool.exe timestamp /t %%s "%~1"
	if ERRORLEVEL 0 if not ERRORLEVEL 1 GOTO success
)
exit /b 1

:success
signtool.exe verify /pa /v "%~1"
exit /b %ERRORCODE%