include FileUtils
include FileTest

class MSBuildRunner
	def self.compile(attributes)
		build_config = attributes.fetch(:build_config)
	    solutionFile = attributes[:solutionfile]

	    attributes[:projFile] = solutionFile
	    attributes[:properties] ||= []
	    attributes[:properties] << "Configuration=#{build_config}"
	    attributes[:extraSwitches] = ["maxcpucount:2", "v:m", "t:rebuild"]

		self.runProjFile(attributes);
	end

	def self.runProjFile(attributes)
		version = attributes.fetch(:clrversion, 'v4.0.30319')
		build_config = attributes.fetch(:build_config, 'debug')
	    projFile = attributes[:projFile]

		frameworkDir = File.join(ENV['windir'].dup, 'Microsoft.NET', 'Framework', version)
		msbuildFile = File.join(frameworkDir, 'msbuild.exe')

		properties = attributes.fetch(:properties, [])

		switchesValue = ""

		properties.each do |prop|
			switchesValue += " /property:#{prop}"
		end	

		extraSwitches = attributes.fetch(:extraSwitches, [])	

		extraSwitches.each do |switch|
			switchesValue += " /#{switch}"
		end	

		targets = attributes.fetch(:targets, [])
		targetsValue = ""
		targets.each do |target|
			targetsValue += " /t:#{target}"
		end

		Kernel::system("#{msbuildFile} #{projFile} #{targetsValue} #{switchesValue}")
	end
end


def cleanDirectory(dir)
	if exists?(dir)
		puts 'Cleaning directory ' + dir
		FileUtils.rm_rf dir;
		waitfor { !exists?(dir) }
	end

	if dir == 'artifacts'
		Dir.mkdir 'artifacts'
	end
end

def waitfor(&block)
	checks = 0
	until block.call || checks >10 
		sleep 0.5
		checks += 1
	end
	raise 'waitfor timeout expired' if checks > 10
end


def copyfile(sourceDir, fileName, destDir)
	FileUtils.copy_file(sourceDir + "/" + fileName, destDir + "/" + fileName)
end
  

