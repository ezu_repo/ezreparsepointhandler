#include "stdafx.h"
#include "Parameters.h"


Parameters::Parameters()
{
	command = Command::Undefined;

	path = NULL;
	newTarget = NULL;
	//curTarget = NULL;
}

Parameters::~Parameters()
{
	path = NULL;
	newTarget = NULL;
}

int Parameters::Parse(int argc, wchar_t* argv[])
{
	for (int i = 1; i < argc; ++i)
	{
		BOOL found = FALSE;
		if (argv[i][0] == '-' || argv[i][0] == '/')
		{
			argv[i][0] = '-';
			if (command == Command::Undefined)	//accept only one command type param, ignore next ones if already set
			{
				found = AssignCommandParam(argv[i]);
			}
			if (!found && (i + 1 < argc))
			{
				found = AssignParam(argv[i], argv[i + 1]);
				if (found)
					++i;	//skip next arg as it is value for this one
			}
		}
	}

	return CheckParamsValid();
}

BOOL Parameters::AssignCommandParam(wchar_t* arg)
{
	if (wcscmp(arg, L"-c") == 0)
	{
		command = Command::CreateReparsePoint;
	}
	else if (wcscmp(arg, L"-o") == 0)
	{
		command = Command::OverwriteReparsePoint;
	}
	else if (wcscmp(arg, L"-s") == 0)
	{
		command = Command::SetNewTarget;
	}
	else if (wcscmp(arg, L"-u") == 0)
	{
		command = Command::UpdateTarget;
	}
	else if (wcscmp(arg, L"-g") == 0)
	{
		command = Command::GetCurrentTarget;
	}
	else if (wcscmp(arg, L"-d") == 0)
	{
		command = Command::DeleteReparsePoint;
	}
	else if (wcscmp(arg, L"-?") == 0)
	{
		command = Command::Help;
	}
	else	//unknown command
	{
		return FALSE;
	}

	return TRUE;
}

BOOL Parameters::AssignParam(wchar_t* arg, wchar_t* val)
{
	if (wcscmp(arg, L"-p") == 0)
	{
		path = val;
		pathEx = std::wstring(L"\\\\?\\").append(std::wstring(path));
		return TRUE;
	}
	else if (wcscmp(arg, L"-t") == 0)
	{
		newTarget = val;
		newTargetEx = std::wstring(L"\\\\?\\").append(std::wstring(newTarget));
		return TRUE;
	}

	return FALSE;
}

//BOOL Parameters::SetLpcwstr(LPCWSTR* target, char* val)
//{
//	if (*target != NULL)
//		delete *target;
//
//	int len = strlen(val) + 1;
//	wchar_t* wstr = new wchar_t[len];
//	int res = mbstowcs(wstr, val, len);
//	*target = wstr;
//
//	return res > -1;
//}

int Parameters::CheckParamsValid()
{
	switch (command)
	{
		case Undefined:
			command = Command::Help;
			return -1;	//command not set - error
		case CreateReparsePoint:
		case OverwriteReparsePoint:
		case SetNewTarget:
		case UpdateTarget:
			if (newTarget == NULL)
				return -3;	//we need target path value to be set
			break;
		case GetCurrentTarget:
		case DeleteReparsePoint:
		default:
			break;
	}

	if (path == NULL)
		return -2;	//we always need Path, error if not provided

	return 0;
}

Command Parameters::GetCommand()
{
	return command;
}
LPCWSTR Parameters::GetPath()
{
	return path;
}

LPCWSTR Parameters::GetPathEx()
{
	return pathEx.c_str();
}

LPCWSTR Parameters::GetNewTarget()
{
	return newTarget;
}
LPCWSTR Parameters::GetNewTargetEx()
{
	return newTargetEx.c_str();
}
//LPCWSTR Parameters::GetCurTarget()
//{
//	return curTarget;
//}
