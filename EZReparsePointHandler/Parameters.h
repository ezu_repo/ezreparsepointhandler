#pragma once
#include "stdafx.h"
#include "windows.h"
#include <iostream>

enum Command
{
	Undefined = 0,
	CreateReparsePoint,
	GetCurrentTarget,
	SetNewTarget,
	UpdateTarget,
	OverwriteReparsePoint,
	DeleteReparsePoint,
	Help
};

class Parameters
{
public:
	Parameters();
	~Parameters();

	int Parse(int argc, wchar_t* argv[]);

	LPCWSTR GetPath();
	LPCWSTR GetPathEx();
	Command GetCommand();

	LPCWSTR GetNewTarget();
	LPCWSTR GetNewTargetEx();
	//LPCWSTR GetCurTarget();
	
private:
	LPCWSTR path;
	std::wstring pathEx;
	Command command;

	LPCWSTR newTarget;
	std::wstring newTargetEx;
	//LPCWSTR curTarget;

	BOOL AssignCommandParam(wchar_t* arg);
	BOOL AssignParam(wchar_t* arg, wchar_t* val);
	//BOOL SetLpcwstr(LPCWSTR* target, char* val);
	int CheckParamsValid();
};
