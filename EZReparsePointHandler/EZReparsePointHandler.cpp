// EZReparsePointHandler.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "windows.h"
#include "iostream"
#include "comdef.h"

#include "Parameters.h"
#include "ReparsePoint.h"

using namespace std;

int _tmain(int argc, wchar_t* argv[])
{
	ReparsePoint rp(argc, argv);
	int result = rp.ExecuteCommand();

	switch (result)
	{
		case 0: 
			cout << "Success" << endl;
			break;
		case -1:
			cout << "Missing Command argument" << endl << endl;
			rp.PrintHelp();
			break;
		case -2:
			cout << "Missing Reparse Point Path argument" << endl << endl;
			rp.PrintHelp();
			break;
		case -3:
			cout << "Missing Reparse Point Target Path argument" << endl << endl;
			rp.PrintHelp();
			break;
		default:
			_com_error error(result);			
			wprintf(L"%s\n", error.ErrorMessage());
			break;
	}

	return result;
}
