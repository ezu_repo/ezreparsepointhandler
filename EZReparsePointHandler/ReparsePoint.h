#pragma once

#include "Parameters.h"
#include <memory>

class ReparsePoint
{
public:
	ReparsePoint(int argc, wchar_t* argv[]);
	~ReparsePoint();

	int ExecuteCommand();
	LPCWSTR GetFoundTargetPath();
	void PrintHelp();

private:
	Parameters params;
	int parseResult;
	HANDLE hFile;
	LPCWSTR* foundTargetPath;

	void CloseFileHandle();
	HANDLE GetHandle(DWORD access, DWORD share, DWORD mode);
	HANDLE GetHandle(DWORD access, DWORD share, DWORD mode, DWORD flagsAndAttrs);
	int ExecuteSetValue(DWORD fileOpenMode);
	int ExecuteGetValue(bool printResult);
	int ExecuteDelete();

	int FileExists(LPCWSTR path);
	int IsDirectory(LPCWSTR path);

	typedef struct _REPARSE_DATA_BUFFER {
		ULONG  ReparseTag;
		USHORT ReparseDataLength;
		USHORT Reserved;
		union {
			struct {
				USHORT SubstituteNameOffset;
				USHORT SubstituteNameLength;
				USHORT PrintNameOffset;
				USHORT PrintNameLength;
				ULONG  Flags;
				WCHAR  PathBuffer[1];
			} SymbolicLinkReparseBuffer;
			struct {
				USHORT SubstituteNameOffset;
				USHORT SubstituteNameLength;
				USHORT PrintNameOffset;
				USHORT PrintNameLength;
				WCHAR  PathBuffer[1];
			} MountPointReparseBuffer;
			struct {
				UCHAR DataBuffer[1];
			} GenericReparseBuffer;
		};
	} REPARSE_DATA_BUFFER;
};

