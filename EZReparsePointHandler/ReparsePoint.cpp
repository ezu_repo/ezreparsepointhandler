#include "stdafx.h"
#include "ReparsePoint.h"
#include "Shlwapi.h"
#include <iostream>
#include <stdio.h>

using namespace std;

ReparsePoint::ReparsePoint(int argc, wchar_t* argv[])
{
	parseResult = params.Parse(argc, argv);
	hFile = NULL;
	foundTargetPath = NULL;
}

ReparsePoint::~ReparsePoint()
{
	delete[] foundTargetPath;
	foundTargetPath = NULL;

	CloseFileHandle();
}

int ReparsePoint::ExecuteCommand()
{
	if (parseResult < 0)
		return parseResult;

	switch (params.GetCommand())
	{
		case Command::CreateReparsePoint:
			return ExecuteSetValue(CREATE_NEW);
		case Command::OverwriteReparsePoint:
			return ExecuteSetValue(CREATE_ALWAYS);
		case Command::SetNewTarget:
		case Command::UpdateTarget:
			return ExecuteSetValue(OPEN_EXISTING);
		case Command::GetCurrentTarget:
			return ExecuteGetValue(true);
		case Command::DeleteReparsePoint:
			return ExecuteDelete();
		case Command::Help:
			PrintHelp();
			return 0;
		case Command::Undefined:
		default:
			return -1;
	}
}

void ReparsePoint::CloseFileHandle()
{
	if (hFile != NULL)
	{
		if (hFile != INVALID_HANDLE_VALUE)
			CloseHandle(hFile);
		hFile = NULL;
	}
}
HANDLE ReparsePoint::GetHandle(DWORD access, DWORD share, DWORD mode)
{
	return GetHandle(access, share, mode, FILE_ATTRIBUTE_NORMAL);
}
HANDLE ReparsePoint::GetHandle(DWORD access, DWORD share, DWORD mode, DWORD flagsAndAttrs)
{
	CloseFileHandle();
	if (hFile == NULL)
	{
		hFile = CreateFileW(params.GetPathEx(), access, share, 0, mode, flagsAndAttrs, 0);
		if (hFile == INVALID_HANDLE_VALUE)
			hFile = NULL;
	}

	return hFile;
}

int ReparsePoint::ExecuteGetValue(bool printResult)
{
	DWORD attrs = GetFileAttributesW(params.GetPathEx());
	if (attrs == INVALID_FILE_ATTRIBUTES)
		return GetLastError();
	if ((attrs & FILE_ATTRIBUTE_REPARSE_POINT) == 0)
		return ERROR_NOT_A_REPARSE_POINT;

	DWORD share = FILE_SHARE_READ | FILE_SHARE_WRITE | FILE_SHARE_DELETE;
	DWORD flagsAndAttrs = FILE_FLAG_BACKUP_SEMANTICS | FILE_ATTRIBUTE_REPARSE_POINT;// | FILE_FLAG_OPEN_REPARSE_POINT;
	HANDLE handle = GetHandle(FILE_READ_ATTRIBUTES, share, OPEN_EXISTING, flagsAndAttrs);
	if (handle == NULL)
		return GetLastError();

	BOOL success = FALSE;
	DWORD allocSize = MAX_PATH;
	WCHAR* path = new WCHAR[allocSize];
	DWORD size = GetFinalPathNameByHandleW(handle, path, allocSize, 0);	//this supposedly works only for Vista and higher
	if (size > 0)	//0 is for failure, positive is size of string
	{
		if (size > allocSize)						//if returned value is bigger than buffer - buffer is too small
		{
			delete[] path;
			path = new WCHAR[allocSize = size];		//allocate new, properly sized buffer

			size = GetFinalPathNameByHandleW(handle, path, allocSize, 0);	//arg is buf size without null terminator
		}
		success = size > 0;

		if (success)
		{
			if (foundTargetPath != NULL)
				delete[] foundTargetPath;
			foundTargetPath = new LPCWSTR(path + 4);	//+4 because string starts with \\?\

			if (printResult)
				wprintf(L"%s\n", *foundTargetPath);
		}
	}

	delete[] path;
	CloseFileHandle();
	return success ? 0 : GetLastError();
}

LPCWSTR ReparsePoint::GetFoundTargetPath()
{
	if (foundTargetPath == NULL)
	{
		int res = ExecuteGetValue(false);
		if (res > 0)	//target path acquisition failed
			return NULL;
	}

	return *foundTargetPath;
}

int ReparsePoint::FileExists(LPCWSTR path)
{
	DWORD attrs = GetFileAttributesW(path);
	if (attrs == INVALID_FILE_ATTRIBUTES)
	{
		int lastError = GetLastError();
		if (lastError == ERROR_FILE_NOT_FOUND ||
			lastError == ERROR_PATH_NOT_FOUND)
		{
			return 0;
		}
		
		return -1;
	}

	return 1;
}
int ReparsePoint::IsDirectory(LPCWSTR path)
{
	DWORD attrs = GetFileAttributesW(path);
	if (attrs == INVALID_FILE_ATTRIBUTES)
		return -1;

	return (attrs & FILE_ATTRIBUTE_DIRECTORY) > 0 ? 1 : 0;
}

int ReparsePoint::ExecuteSetValue(DWORD fileOpenMode)
{
	int isDir = IsDirectory(params.GetNewTargetEx());
	if (isDir < 0)
		return GetLastError();
	
	int fileExists = FileExists(params.GetPathEx());
	if (fileExists == 0)
	{
		if (fileOpenMode == OPEN_EXISTING)
			return isDir ? ERROR_PATH_NOT_FOUND : ERROR_FILE_NOT_FOUND;
	}
	else if (fileExists > 0)
	{
		if (fileOpenMode == CREATE_NEW)
			return ERROR_ALREADY_EXISTS;

		int delRes = ExecuteDelete();
		if (delRes > 0)
			return delRes;
	}
	else
		return GetLastError();

	if (CreateSymbolicLinkW(params.GetPath(), params.GetNewTarget(), 
							isDir ? SYMBOLIC_LINK_FLAG_DIRECTORY : 0))
	{
		return 0;
	}

	return GetLastError();
}

int ReparsePoint::ExecuteDelete()
{
	DWORD attrs = GetFileAttributesW(params.GetPathEx());
	if (attrs == INVALID_FILE_ATTRIBUTES)
		return GetLastError();

	if ((attrs & FILE_ATTRIBUTE_REPARSE_POINT) == 0)
		return ERROR_NOT_A_REPARSE_POINT;

	BOOL success = FALSE;
	if ((attrs & FILE_ATTRIBUTE_DIRECTORY) == 0)
		success = DeleteFileW(params.GetPathEx());
	else
		success = RemoveDirectoryW(params.GetPathEx());

	return success ? 0 : GetLastError();
}

void ReparsePoint::PrintHelp()
{
	cout << "Parameters: " << endl;
	cout << "-c Command: CreateReparsePoint" << endl;
	cout << "-o Command: OverwriteReparsePoint" << endl;
	cout << "-s Command: SetNewTarget" << endl;
	cout << "-u Command: UpdateTarget" << endl;
	cout << "-g Command: GetCurrentTarget" << endl;
	cout << "-d Command: DeleteReparsePoint" << endl << endl;
	cout << "-p \"<Reparse Point Path>\"" << endl;
	cout << "-t \"<Reparse Point Target Path>\"" << endl;
}
