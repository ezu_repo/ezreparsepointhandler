TARGET_FRAMEWORK_VERSION = "v4.0"

require File.dirname(__FILE__) + "/build_support/BuildUtils.rb"
require 'albacore'


props = {
	:build_support => File.expand_path("build_support"),
	:stage => File.expand_path("build_output"),
	:stage_cmd => File.expand_path("build_output").gsub('/', '\\'),
	:keyfile => File.expand_path("EZReparsePointHandler.snk"),
	:solution => File.expand_path("EZReparsePointHandler.sln"),
	:commonresourceheader => File.expand_path("CommonResourceHeader.h"),
}

if File.exists?("VERSION.txt")
	load "VERSION.txt"
elsif ENV["EZRPH_BUILD_VERSION"] != nil  
	EZRPH_BUILD_VERSION = ENV["EZRPH_BUILD_VERSION"]
else
	EZRPH_BUILD_VERSION = "1.0.0"
end

build_version = "#{EZRPH_BUILD_VERSION}"
begin      
	build_version = "#{EZRPH_BUILD_VERSION_DEVELOPMENT}" if `git rev-parse --symbolic-full-name --abbrev-ref HEAD`.chomp == 'development'    
rescue      
end

ci_build_number = ENV["BUILD_NUMBER"]
build_revision = ci_build_number || Time.new.strftime('5%H%M')
build_number = build_version + ".#{build_revision}"

desc "**Default**, compiles and runs tests"
task :default => [:compile]

task :version do
    sh ">\"#{props[:commonresourceheader]}\"  echo #define VS_FILE_VERSION_VAL  #{build_number.gsub('.', ',')}"
	sh ">>\"#{props[:commonresourceheader]}\" echo #define VS_FILE_VERSION_STR \"#{build_number}\""
end


task :clean do
	cleanDirectory(props[:stage])
end


task :compile => [:clean, :version, :build_ezrph, :publish_ezrph] do
  
end

task :default => [:compile] do
end

msbuild :build_ezrph do |msb|
    msb.properties :Configuration => 'Release',
        #:TargetFrameworkVersion => TARGET_FRAMEWORK_VERSION,
        :Platform => 'Win32'   

    msb.targets :Clean, :Build
    msb.properties[:SignAssembly] = 'true'
    msb.properties[:AssemblyOriginatorKeyFile] = props[:keyfile]
    msb.solution = props[:solution]
end

task :publish_ezrph do
	sh "if not exist \"#{props[:stage_cmd]}\" md \"#{props[:stage_cmd]}\""
	sh "copy /Y \"Release\\EZReparsePointHandler.exe\" \"#{props[:stage_cmd]}\\EZReparsePointHandler.exe\""
	sh "copy /Y \"Release\\EZReparsePointHandler.pdb\" \"#{props[:stage_cmd]}\\EZReparsePointHandler.pdb\""
end
